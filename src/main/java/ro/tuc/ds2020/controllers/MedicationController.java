package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.services.MedicationService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    public MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @PostMapping
    public ResponseEntity<MedicationDTO> insertMedication (@Valid @RequestBody MedicationDTO medicationDTO) {
        MedicationDTO medicationDTO1 = MedicationBuilder.toMedicationDTO(medicationService.insert(MedicationBuilder.toMedication(medicationDTO)));
        return new ResponseEntity<>(medicationDTO1, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<MedicationDTO>> getMedication() {
        return new ResponseEntity<>(medicationService.showMedications(), HttpStatus.OK);
    }


    @GetMapping(value = "/find-user/{name}")
    public ResponseEntity<MedicationDTO> getMedicationByName(@PathVariable("name") String name) {
        return new ResponseEntity<>(medicationService.getMedicationByName(name), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> getMedicationById(@PathVariable("id") UUID id) {
        return new ResponseEntity<>(medicationService.getMedicationById(id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UUID> deleteMedication (@PathVariable("id") UUID id) {
        return new ResponseEntity<>(medicationService.deleteMedication(id), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<MedicationDTO> updateMedication(@Valid @RequestBody MedicationDTO medicationDTO) {
        return new ResponseEntity<>(MedicationBuilder.toMedicationDTO(medicationService.updateMedication(MedicationBuilder.toMedication(medicationDTO))), HttpStatus.OK);
    }

}
