package ro.tuc.ds2020.dtos;

import javax.persistence.Column;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class CaregiverDTO {

    private UUID id;
    private String name;
    private String address;
    private String birthdate ;
    private String gender ;
    private String username;
    private String password;

    private List<PatientDTO> listOfPatients;

    public CaregiverDTO() {
    }

    public CaregiverDTO(UUID id, String name, String address, String birthdate, String gender, String username, String password, List<PatientDTO> listOfPatients) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.birthdate = birthdate;
        this.gender = gender;
        this.username = username;
        this.password = password;
        this.listOfPatients = listOfPatients;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<PatientDTO> getListOfPatients() {
        return listOfPatients;
    }

    public void setListOfPatients(List<PatientDTO> listOfPatients) {
        this.listOfPatients = listOfPatients;
    }
}
