package ro.tuc.ds2020.dtos;

import javax.persistence.Column;

public class MedicalRecordDTO {

    private int id;
    private String description;

    public MedicalRecordDTO(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public MedicalRecordDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
