package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Medication;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public class MedicationPlanDTO {

    private UUID id;
    private List<MedicationDTO> medicationList;
    private String intakeIntervals;
    private String treatmentPeriod;

    public MedicationPlanDTO(UUID id, List<MedicationDTO> medicationList, String intakeIntervals, String treatmentPeriod) {
        this.id = id;
        this.medicationList = medicationList;
        this.intakeIntervals = intakeIntervals;
        this.treatmentPeriod = treatmentPeriod;
    }

    public MedicationPlanDTO() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<MedicationDTO> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<MedicationDTO> medicationList) {
        this.medicationList = medicationList;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getTreatmentPeriod() {
        return treatmentPeriod;
    }

    public void setTreatmentPeriod(String treatmentPeriod) {
        this.treatmentPeriod = treatmentPeriod;
    }
}

