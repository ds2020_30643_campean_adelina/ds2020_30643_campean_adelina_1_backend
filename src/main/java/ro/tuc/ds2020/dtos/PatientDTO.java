package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.MedicalRecord;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class PatientDTO {

    private UUID id;
    private String name;
    private String address;
    private String birthdate ;
    private String gender ;
    private String username;
    private String password;
    private String medicalRecord;
    private List<MedicationPlanDTO> medicationPlans;

    public PatientDTO() {
    }

    public PatientDTO(UUID id, String name, String address, String birthdate, String gender, String username, String password, String medicalRecord, List<MedicationPlanDTO> medicationPlans) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.birthdate = birthdate;
        this.gender = gender;
        this.username = username;
        this.password = password;
        this.medicalRecord = medicalRecord;
        this.medicationPlans = medicationPlans;
    }

    public List<MedicationPlanDTO> getMedicationPlans() {
        return medicationPlans;
    }

    public void setMedicationPlans(List<MedicationPlanDTO> medicationPlans) {
        this.medicationPlans = medicationPlans;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
