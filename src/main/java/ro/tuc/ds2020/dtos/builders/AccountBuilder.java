package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Patient;

import java.text.SimpleDateFormat;

public class AccountBuilder {

    public AccountBuilder() {
    }

    public static AccountDTO toAccountDTO (Account account) {

        return new AccountDTO(account.getId(),account.getRole(), account.getUsername(), account.getPassword());
    }


    public static Account toPatient (AccountDTO accountDTO) {

        return new Account(accountDTO.getId(), accountDTO.getRole(), accountDTO.getUsername(), accountDTO.getPassword());
    }
}
