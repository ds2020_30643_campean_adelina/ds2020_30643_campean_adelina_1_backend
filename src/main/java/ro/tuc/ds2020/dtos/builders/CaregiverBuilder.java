package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Collectors;

public class CaregiverBuilder {

    private static PatientBuilder patientBuilder = new PatientBuilder();

    public CaregiverBuilder() {
    }

    public static CaregiverDTO toCaregiverDTO (Caregiver caregiver) {
        return new CaregiverDTO(caregiver.getId(), caregiver.getName(), caregiver.getAddress(), caregiver.getBirthdate().toString(), caregiver.getGender(), caregiver.getUsername(), caregiver.getPassword(), caregiver.getListOfPatients().stream().map(patient -> patientBuilder.toPatientDTO(patient)).collect(Collectors.toList()));
    }

    public static Caregiver toCaregiver (CaregiverDTO caregiverDTO) {

        try {
            return new Caregiver(caregiverDTO.getId(), caregiverDTO.getName(), caregiverDTO.getAddress(), new SimpleDateFormat("MM.dd.yyyy").parse(caregiverDTO.getBirthdate()), caregiverDTO.getGender(), caregiverDTO.getUsername(), caregiverDTO.getPassword(), caregiverDTO.getListOfPatients().stream().map(patientDTO -> patientBuilder.toPatient(patientDTO)).collect(Collectors.toList()));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
