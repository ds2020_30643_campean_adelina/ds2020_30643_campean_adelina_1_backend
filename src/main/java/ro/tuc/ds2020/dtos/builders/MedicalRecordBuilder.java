package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicalRecordDTO;
import ro.tuc.ds2020.entities.MedicalRecord;

public class MedicalRecordBuilder {


    public static MedicalRecordDTO toMedicalRecordDTO (MedicalRecord md) {
        return new MedicalRecordDTO(md.getId(), md.getDescription());
    }

    public static MedicalRecord toMedicalRecord (MedicalRecordDTO mdDTO) {
        return new MedicalRecord(mdDTO.getId(), mdDTO.getDescription());
    }

}
