package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.MedicationPlan;

import java.util.stream.Collectors;

public class MedicationPlanBuilder {

    private static MedicationBuilder medicationBuilder = new MedicationBuilder();

    public MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(medicationPlan.getId(),medicationPlan.getMedicationList().stream().map(medication -> medicationBuilder.toMedicationDTO(medication)).collect(Collectors.toList()), medicationPlan.getIntakeIntervals(),medicationPlan.getTreatmentPeriod());
    }

    public static MedicationPlan toMedicationPlan(MedicationPlanDTO medicationPlanDTO) {
        return new MedicationPlan(medicationPlanDTO.getId(), medicationPlanDTO.getMedicationList().stream().map(medicationDTO -> medicationBuilder.toMedication(medicationDTO)).collect(Collectors.toList()), medicationPlanDTO.getIntakeIntervals(),medicationPlanDTO.getTreatmentPeriod());
    }


}
