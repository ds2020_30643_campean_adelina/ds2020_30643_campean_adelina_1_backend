package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Patient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.stream.Collectors;

public class PatientBuilder {

    private static MedicationPlanBuilder medicationPlanBuilder = new MedicationPlanBuilder();

    public PatientBuilder() {
    }

    public static PatientDTO toPatientDTO (Patient patient) {

        return new PatientDTO(patient.getId(), patient.getName(), patient.getAddress(), patient.getBirthdate().toString(), patient.getGender(), patient.getUsername(), patient.getPassword(), patient.getMedicalRecord(), patient.getMedicationPlans().stream().map(medicationPlan -> medicationPlanBuilder.toMedicationPlanDTO(medicationPlan)).collect(Collectors.toList()));
    }


    public static Patient toPatient (PatientDTO patientDTO) {
        try {
            if(patientDTO.getMedicationPlans() != null)
                return new Patient(patientDTO.getId(), patientDTO.getName(), patientDTO.getAddress(), new SimpleDateFormat("MM.dd.yyyy").parse(patientDTO.getBirthdate()), patientDTO.getGender(), patientDTO.getUsername(),patientDTO.getPassword(), patientDTO.getMedicalRecord(), patientDTO.getMedicationPlans().stream().map(medicationPlanDTO -> medicationPlanBuilder.toMedicationPlan(medicationPlanDTO)).collect(Collectors.toList()));
            else
                return new Patient(patientDTO.getId(), patientDTO.getName(), patientDTO.getAddress(), new SimpleDateFormat("MM.dd.yyyy").parse(patientDTO.getBirthdate()), patientDTO.getGender(), patientDTO.getUsername(),patientDTO.getPassword(), patientDTO.getMedicalRecord(), null);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}


