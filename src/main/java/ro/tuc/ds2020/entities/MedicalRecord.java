package ro.tuc.ds2020.entities;

import javax.persistence.*;

@Table
@Entity
public class MedicalRecord {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "description", nullable = false)
    private String description;


    public MedicalRecord() {
    }

    public MedicalRecord(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

