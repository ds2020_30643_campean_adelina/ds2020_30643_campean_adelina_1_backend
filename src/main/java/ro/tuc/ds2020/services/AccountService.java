package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.builders.AccountBuilder;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.repositories.AccountRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account insertAccount (Account account) {
        return accountRepository.save(account);
    }

    public Account updateAccount (Account account) {
        List<Account> accountList = accountRepository.findAll();
        Account a = new Account();
        for(Account account1: accountList)
        {
            if(account1.getId().equals(account.getId()))
            {
                a = account1;
                break;
            }
        }
        a.setUsername(account.getUsername());
        a.setPassword(account.getPassword());
        a.setRole(account.getRole());

        accountRepository.save(a);

        return a;
    }

    public List<AccountDTO> getAccounts() {
        return accountRepository.findAll().stream().map(account -> AccountBuilder.toAccountDTO(account)).collect(Collectors.toList());
    }

    public String updateAcc (String username, String usernameNew, String password) {
        List<Account> accountList = accountRepository.findAll();

        Account a = new Account();
        for(Account account1: accountList) {
            if (account1.getUsername().equals(username)) {
                a = account1;

                break;
            }
        }

        a.setUsername(usernameNew);
        a.setPassword(password);

        accountRepository.save(a);

        return usernameNew;
    }

    public Account getByUsername (String username) {
        List<Account> accountList = accountRepository.findAll();

        Account a = new Account();
        for(Account account1: accountList) {
            if (account1.getUsername().equals(username)) {
                a = account1;

                break;
            }
        }
        return a;
    }



}
