package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;
    private final PatientRepository patientRepository;
    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, PatientRepository patientRepository) {
        this.caregiverRepository = caregiverRepository;
        this.patientRepository = patientRepository;
    }

    public Caregiver insert(Caregiver caregiver) {
        return caregiverRepository.save(caregiver);
    }

    public List<CaregiverDTO> showCaregivers() {

        List<Caregiver> caregivers = caregiverRepository.findAll();
        return caregivers.stream().map(caregiver -> CaregiverBuilder.toCaregiverDTO(caregiver)).collect(Collectors.toList());
    }

    public CaregiverDTO getCaregiverById(UUID id) {
       List<CaregiverDTO> caregivers = showCaregivers();
        CaregiverDTO cdto = new CaregiverDTO();
        for(CaregiverDTO caregiverDTO:caregivers) {
            if(caregiverDTO.getId().equals(id)) {
                cdto = caregiverDTO;
                break;
            }
        }
        return cdto;
    }

    public CaregiverDTO getCaregiverByUsername(String username) {
        List<CaregiverDTO> caregivers = showCaregivers();
        CaregiverDTO cdto = new CaregiverDTO();
        for(CaregiverDTO caregiverDTO:caregivers) {
            if(caregiverDTO.getUsername().equals(username)) {
                cdto = caregiverDTO;
                break;
            }
        }
        return cdto;
    }

    public Caregiver delete(Caregiver caregiver) {
        caregiverRepository.deleteById(caregiver.getId());
        return caregiver;
    }

    public UUID deleteCaregiverById(UUID id) {
        caregiverRepository.deleteById(id);
        return id;
    }

    public Caregiver update(Caregiver caregiver) {
        List<Caregiver> caregivers = caregiverRepository.findAll();
        Caregiver c = new Caregiver();
        for(Caregiver caregiver1:caregivers) {
            if (caregiver1.getId().equals(caregiver.getId())) {
                c = caregiver1;
                break;
            }
        }

        if (!caregiver.getAddress().isEmpty()) {
            c.setAddress(caregiver.getAddress());
        }

        if(!caregiver.getBirthdate().toString().isEmpty()) {
            c.setBirthdate(caregiver.getBirthdate());
        }

        if(!caregiver.getGender().isEmpty()) {
            c.setGender(caregiver.getGender());
        }

        if(!caregiver.getName().isEmpty()) {
            c.setName(caregiver.getName());
        }

        if(!caregiver.getUsername().isEmpty()) {
            c.setUsername(caregiver.getUsername());
        }

        if(!caregiver.getPassword().isEmpty()) {
            c.setPassword(caregiver.getPassword());
        }


      /* List<Patient> pacienti = caregiver.getListOfPatients();
       // List<Patient> p1 = new ArrayList<Patient>();

        List<Patient> p1 = c.getListOfPatients();

        for(Patient p4 :pacientiExistenti)
        {
            p1.add(p4);
        }

        List<Patient> patient2;
        for(Patient p: pacienti)
        {
            patient2 =  patientRepository.findAll();
           for(Patient p3: patient2)
           {
              if(p3.getId().equals(p.getId())){
                   p1.add(p3);
               }
           }
        }
*/
        List<Patient> pacienti = caregiver.getListOfPatients();
        List<Patient> p1 = c.getListOfPatients();
        for(Patient p4 :p1)
        {
            pacienti.add(p4);
        }
        c.setListOfPatients(pacienti);

        caregiverRepository.save(c);

        return c;
    }

}
