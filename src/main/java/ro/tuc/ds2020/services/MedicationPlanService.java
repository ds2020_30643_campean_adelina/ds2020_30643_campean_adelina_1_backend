package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final MedicationPlanRepository medicationPlanRepository;
    private MedicationService medicationService;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public MedicationPlan insert(MedicationPlan medicationPlan) {
        return medicationPlanRepository.save(medicationPlan);
    }

    public List<MedicationPlanDTO> showMedicationPlan() {
        List<MedicationPlan> medicationPlans = medicationPlanRepository.findAll();
        return medicationPlans.stream().map(medicationPlan -> MedicationPlanBuilder.toMedicationPlanDTO(medicationPlan)).collect(Collectors.toList());
    }

    public List<MedicationDTO> getMedsMedicalPlan(UUID idMedPlan ) {

        List<MedicationPlanDTO> medicationPlanDTOS = showMedicationPlan();

        MedicationPlanDTO medicationPlanDTO = new MedicationPlanDTO();

        for(MedicationPlanDTO mpd : medicationPlanDTOS) {
            if(mpd.getId().equals(idMedPlan))
            {
                medicationPlanDTO = mpd;
                break;
            }
        }

        List<MedicationDTO> meds = medicationPlanDTO.getMedicationList();

        return meds;
    }


}
