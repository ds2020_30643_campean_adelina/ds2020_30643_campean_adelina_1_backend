package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public Medication insert(Medication medication) {

        return medicationRepository.save(medication);
    }

    public List<MedicationDTO> showMedications() {
        List<Medication> medications = medicationRepository.findAll();
        return medications.stream().map(medication -> MedicationBuilder.toMedicationDTO(medication)).collect(Collectors.toList());
    }

    public UUID deleteMedication(UUID id) {
        medicationRepository.deleteById(id);
        return id;
    }

    public MedicationDTO getMedicationById(UUID id) {
        List<MedicationDTO> medications = showMedications();
        MedicationDTO medicationDTO = new MedicationDTO();
        for(MedicationDTO medDTO:medications) {
            if(medDTO.getId().equals(id)) {
                medicationDTO = medDTO;
                break;
            }
        }
        return medicationDTO;
    }

    public MedicationDTO getMedicationByName(String name) {
        List<MedicationDTO> medications = showMedications();
        MedicationDTO medicationDTO = new MedicationDTO();
        for(MedicationDTO medDTO:medications) {
            if(medDTO.getName().equals(name)) {
                medicationDTO = medDTO;
                break;
            }
        }
        return medicationDTO;
    }


    public Medication updateMedication(Medication medication) {
        List<Medication> medications = medicationRepository.findAll();
        Medication m = new Medication();
        for(Medication med: medications) {
            if(med.getId().equals(medication.getId())) {
                m = med;
                break;
            }
        }

        if(!medication.getName().isEmpty())
            m.setName(medication.getName());

        if(!medication.getSideEffects().isEmpty())
            m.setSideEffects(medication.getSideEffects());

        if(!medication.getDosage().isEmpty())
            m.setDosage(medication.getDosage());

        medicationRepository.save(m);

        return m;
    }

}
